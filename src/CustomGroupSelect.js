import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import ListSubheader from '@material-ui/core/ListSubheader';
import PropTypes from 'prop-types';
import { TextField } from 'formik-material-ui';
import { Field }  from 'formik';

const CustomGroupSelect = ({inputs}) => (
    inputs.map(input => (
            <Field
                fullWidth
                key={input.name}
                name={input.name}
                type='text'
                label={input.label}
                select
                variant='standard'
                component={TextField}

            >
              {
                input.items.map(
                    ({label, options}) => ([
                          <ListSubheader key={label}>{label}</ListSubheader>,
                          options.map(({value, label}) => <MenuItem key={value} value={value}>{label}</MenuItem>)
                        ]
                    )
                )}
            </Field>
        )
    )
);

CustomGroupSelect.propTypes = {
  input: PropTypes.object,
};

export default CustomGroupSelect;
