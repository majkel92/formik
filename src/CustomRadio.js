import React from 'react';
import {RadioGroup} from 'formik-material-ui';
import {FormControlLabel, makeStyles, Radio} from '@material-ui/core';
import FormHelperText from '@material-ui/core/FormHelperText';
import {Field} from 'formik';

const useStyle = makeStyles(theme => ({
  formHelperText: {color: '#f44336'},
}));

const InputFeedback = ({error}) => {
  const classes = useStyle();
  return (
      error ? <FormHelperText className={classes.formHelperText}>{error}</FormHelperText> : null
  )
};

const CustomRadio = ({inputs, form:{touched, errors}}) => {
  return (
    inputs.map(input => (
        <Field key={input.name} name={input.name} component={RadioGroup}>
          <FormControlLabel
              value="female"
              control={<Radio color="primary"/>}
              label="Female"

          />
          <FormControlLabel
              value="male"
              control={<Radio color="primary"/>}
              label="Male"

          />
          <FormControlLabel
              value="other"
              control={<Radio color="primary"/>}
              label="Other"

          />
          {touched[input.name] && <InputFeedback error={errors[input.name]}/>}
        </Field>
    ))
  )
};

export default CustomRadio;