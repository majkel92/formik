import React from 'react'
import {Formik, Form, Field} from 'formik'
import {TextField} from 'formik-material-ui'
import Checkbox from './Checkbox'
import {DatePicker} from 'material-ui-formik-components/DatePicker';
import {MuiPickersUtilsProvider} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import LocalePl from 'date-fns/esm/locale/pl'
import {object, string, mixed, addMethod, bool} from 'yup'
import CustomGroupSelect from './CustomGroupSelect';
import CustomRadio from './CustomRadio';
import CustomSelect from './CustomSelect';


class App extends React.PureComponent {
  render() {
    const taxInputs = [
      {
        type: 'selectGroup',
        items: [
          {
            label: 'Altany i obiekty budowlane i obiekty gospodarcze',
            options: [{value: 'art7ust1pkt12', label: 'art. 7 ust 1 pkt 12'}]
          },
          {label: 'Inne - budowle wałów ochronnych', options: [{value: 'art7ust1pkt9', label: 'art. 7 ust 1 pkt 9'}]},
          {label: 'Działalność rolna', options: [{value: 'art7ust1pkt13', label: 'art. 7 ust 1 pkt 13'}]},
          {
            label: 'Infrastruktura portowa i lotnicza',
            options: [{value: 'art7ust1pkt2', label: 'art. 7 ust 1 pkt 2'}, {
              value: 'art7ust1pkt3',
              label: 'art. 7 ust 1 pkt 3'
            }]
          },
          {
            label: 'Infrastruktura kolejowa',
            options: [{value: 'art7ust1pkt1a', label: 'art. 7 ust 1 pkt 1 lit. a)'}, {
              value: 'art7ust1pkt1b',
              label: 'art. 7 ust 1 pkt 1 lit. b)'
            }, {value: 'art7ust1pkt1c', label: 'art. 7 ust 1 pkt 1 lit. c)'}]
          },
          {label: 'ZPChr', options: [{value: 'art7ust2pkt4', label: 'art. 7 ust 2 pkt 4'}]}
        ],
        name: 'zwolnieniePodatkowe',
        label: 'Zwolnienie podatkowe',
      },
    ];

    const formOfPossessionInputs = [{
      type: 'select',
      items: [
        {value: 'wlasnosc', label: 'Własność'},
        {value: 'uzytkowanieWieczyste', label: 'Użytkowanie wieczyste'},
        {value: 'posiadanieSamoistne', label: 'Posiadanie samoistne'},
        {value: 'posiadnaieZalezne', label: 'Posiadanie zależne'},
        {value: 'posiadanieBezTytuluPrawnego', label: 'Posiadanie bez tytułu prawnego'},
      ],
      name: 'select',
      label: 'Forma władania',
    }];

    const buildTimeInputs = [
      {
        type: 'checkbox',
        name: 'garazWybudowanyWTymSamymCzasieCoDom',
        label: 'Garaż budowany w tym samym czasie co dom',
      },
    ];

    //dodanie customowej walidacji
    addMethod(mixed, 'contain', function (v, message) {
      const msg = message;
      return this.test('contain', msg, function (value) {
        const {path, createError} = this;
        return (value != undefined && !value.includes(v)) || createError({path, msg});
      })
    });

    const validationSchema = object().shape({
      username: string()
          .required('Login jest wymagany')
          .contain('a', 'zawiera a'),
      date: string()
          .required('Musisz wybrać datę')
          .typeError('Musisz wybrać datę'),
      zwolnieniePodatkowe: string()
          .required('Wybierz zwolnienie podatkowe'),
      garazWybudowanyWTymSamymCzasieCoDom: bool()
          .oneOf([true], 'zaznacz checkboxa'),
      select: string()
          .required('dodaj'),
    });

    return (
        <div>
          <h1>Register</h1>
          <MuiPickersUtilsProvider utils={DateFnsUtils} locale={LocalePl}>
            <Formik
                initialValues={{
                  username: '',
                  date: null,
                  zwolnieniePodatkowe: '',
                  formaWladania: '',
                  garazWybudowanyWTymSamymCzasieCoDom: false,
                  select: '',
                }}
                validationSchema={validationSchema}
                validateOnBlur={false}
                onSubmit={values => {
                  alert(`Username: ${values.date}\nGender: ${values.date} \nSelect: ${values.zwolnieniePodatkowe}\nChecked: ${values.checkbox}`)
                }}
                render={(form, field) => (

                    <Form noValidate autoComplete={'off'}>
                      <Field
                          name="username"
                          label="Login"
                          type="text"
                          component={TextField}
                      />
                      <Field
                          clearable
                          type='select'
                          name="date"
                          label="Data"
                          format="dd/MM/yyyy"
                          component={DatePicker}
                      />

                      <CustomGroupSelect
                          inputs={taxInputs}
                      />

                      <Checkbox
                          inputs={buildTimeInputs}
                          form={form}
                      />

                      <CustomSelect
                          inputs={formOfPossessionInputs}
                      />

  {/*                    <CustomRadio
                          inputs={buildTimeInputs}
                          form={form}
                      />*/}

                      <button type="submit">
                        Submit
                      </button>
                    </Form>
                )}
            />
          </MuiPickersUtilsProvider>
        </div>
    )
  }
}

export default App