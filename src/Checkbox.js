import React from 'react';
import FormHelperText from '@material-ui/core/FormHelperText';
import {makeStyles} from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {Field} from 'formik';

const useStyle = makeStyles(theme => ({
  formHelperText: {color: '#f44336'},
}));

const InputFeedback = ({error}) => {
  const classes = useStyle();
  {
    console.log(error)
  }
  return (
      error ? <FormHelperText className={classes.formHelperText}>{error}</FormHelperText> : null
  )
};

const Checkbox1 = ({
                     inputs,
                     form: {errors, touched}
                   }) => {
  return (
      inputs.map(input => (
              <div key={input.name}>
                <Field name={input.name} component={FormControlLabel}
                       control={
                         <Checkbox name={input.name} value={'antoine'}/>
                       }
                       label="Antoine Llorca"
                >
                </Field>
                {touched[input.name] && <InputFeedback error={errors[input.name]}/>}
              </div>
          )
      )
  );
};

export default Checkbox1;